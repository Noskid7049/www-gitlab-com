---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-07-16.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 315   | 100%        |
| Based in the US                           | 181  |  57.46%      |
| Based in the UK                           | 20    | 6.35%     |
| Based in the Netherlands                  | 13    | 4.13%       |
| Based in Other Countries                  | 93    | 32.06%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 315   | 100%        |
| Men                                       | 248   | 78.73%      |
| Women                                     | 66    | 20.95%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 30    | 100%        |
| Men in Leadership                         | 24    | 80.00%      |
| Women in Leadership                       | 6     | 20.00%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 131   | 100%        |
| Men in Development                        | 117   | 89.31%      |
| Women in Development                      | 13    | 9.92%      |
| Other Gender Options                      | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 181   | 100%        |
| Asian                                     | 15    | 8.29%       |
| Black or African American                 | 3     | 1.66%       |
| Hispanic or Latino                        | 12    | 6.63%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.55%       |
| Two or More Races                         | 7     | 3.87%       |
| White                                     | 93    | 51.38%      |
| Unreported                                | 50    | 27.62%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 43    | 100%        |
| Asian                                     | 4     | 9.30%      |
| Black or African American                 | 1     | 2.33%       |
| Hispanic or Latino                          3       6.98%
  Two or More Races                         | 2     | 4.65%       |
| White                                     | 22    | 51.16%      |
| Unreported                                | 11    | 25.58%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 25    | 100%        |
| Asian                                     | 3     | 12.00%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.00%       |
| White                                     | 13    | 52.00%      |
| Unreported                                | 8     | 32.00%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 315   | 100%        |
| Asian                                     | 27    | 8.57%       |
| Black or African American                 | 6     | 1.90%       |
| Hispanic or Latino                        | 19    | 6.03%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.32%       |
| Two or More Races                         | 8     | 2.54%       |
| White                                     | 160   | 50.79%      |
| Unreported                                | 94    | 29.84%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 131   | 100%        |
| Asian                                     | 9     | 6.87%       |
| Black or African American                 | 3     | 2.29%       |
| Hispanic or Latino                        | 9     | 6.87%       |
| Two or More Races                         | 3     | 2.29%       |
| White                                     | 67    | 51.15%      |
| Unreported                                | 40    | 30.53%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 30    | 100%        |
| Asian                                     | 3     | 10.00%       |
| Hispanic or Latino                          1       3.33%
  Native Hawaiian or Other Pacific Islander | 1     | 3.33%       |
| White                                     | 14    | 46.67%      |
| Unreported                                | 11    | 36.67%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 315   | 100%        |
| 18-24                                     | 17    | 5.40%       |
| 25-29                                     | 72    | 22.86%      |
| 30-34                                     | 91    | 28.89%      |
| 35-39                                     | 50    | 15.87%      |
| 40-49                                     | 52    | 16.51%      |
| 50-59                                     | 30    | 9.52%       |
| 60+                                       | 2     | 0.63%       |
| Unreported                                | 1     | 0.32%       |
