---
layout: job_page
title: "Director of Product"
---

As the Director of Product, you will be responsible for managing and building the team that focuses on a subset of GitLab's [product categories](/handbook/product/categories/#devops-lifecycle-stages).

## Individual responsibility

- Make sure you have a great product team (recruit and hire, sense of progress, promote proactively, identify underperformance)
- Work on the vision with the Head of Product, VP of Product, and CEO; and communicate this vision internally and externally
- Distill the overall vision into a compelling roadmap
- Make sure the vision advances in every release and communicate this
- Communicate our vision though demo's, conference speaking, blogging, and interviews
- Work closely with Product Marketing, Sales, Engineering, etc.

## Team responsibility

- Ensure that the next milestone contains the most relevant items to customers, users, and us
- Work with customers, users, and other teams to make feature proposals enticing, actionable, and small
- Make sure the [comparisons](/comparison/) are up to date
- Keep [/direction](/direction) up to date as our high level roadmap
- Regularly join customer and partner visits that can lead to new features
- Ensure that we translate user demands in features that make them happy but keep the product UI clean and the codebase maintainable
- Make sure the release announcements are attractive and cover everything
- Be present on social media (hacker news, twitter, stack overflow, mailinglist), especially around releases

## Requirements

* 3-5 years experience in managing product managers
* 8-10 years of experience in product management
* Technical background or clear understanding of developer products; familiarity with Git, Continuous Integration, Containers, Kubernetes, and Project Management software a plus
* Experience managing a team of product managers
* You share our [values](/handbook/values), and work in accordance with those values

## Specialties

### Ops Products

The Director of Product, Ops Products leads the Ops parts of the [DevOps lifecycle](/product/categories/#devops-lifecycle-stages) (e.g. continuous integration, delivery, and deployment, Prometheus monitoring, and Secure) and reports to the Head of Product.

#### Additional requirements

* Experience in DevOps
* Experience with Docker and Kubernetes

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our recruiters.
* A 45 minute interview with our Head of Product
* A 45 minute interview with one of our technical leads or managers
* A 45 minute interview with one of our Product Managers
* A 45 minute interview with our Vice President of Product
* Optionally, a 45 minute interview with our Chief Executive Officer
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
