---
layout: markdown_page
title: "Financial Services Regulatory Compliance"
---

# Examples (TODO remove this before linking from other pages)

1. https://www.infoq.com/news/2016/07/devops-survival-finance
1. https://www.hpe.com/us/en/insights/articles/how-the-federal-reserve-bank-of-new-york-navigates-the-supply-chain-of-open-source-software-1710.html
1. https://www.hpe.com/us/en/insights/articles/primer-ensuring-regulatory-compliance-in-cloud-deployments-1704.html

## Introduction

GitLab is used extensively to achieve regulatory compliance in the financial services industry.
This page details the relevant rules, the principles needed to achieve them, and the features in GitLab that make that possible.

## Regulators and regulations

1. America: [FEB](https://www.federalreserve.gov/) in the US, also see the [FFIEC IT Handbook](https://ithandbook.ffiec.gov/)
1. Europe: [FCA](https://www.fca.org.uk/) and [PRA](https://www.bankofengland.co.uk/prudential-regulation) in the UK, [FINMA](https://www.finma.ch/en/) in Switzerland
1. Asia: [MAS](http://www.mas.gov.sg/)in Singapore and [HKMA](http://www.hkma.gov.hk/eng/index.shtml)

TODO Add links to relevant sections of the regulations.

## Separation of duties

### Rules

TODO

### Principles

1. You never merge your own code.
1. All code needs to be peer reviewed.
1. Only authorized people can approve the code.
1. You need a log of who approved it

### Features

1. Protected branches https://docs.gitlab.com/ee/user/project/protected_branches.html
1. Merge request approvals https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html
1. Unprotect permission https://about.gitlab.com/2018/04/22/gitlab-10-7-released/#protected-branch-unprotect-permissions
1. Future: Two person rule, https://en.wikipedia.org/wiki/Two-man_rule for admins

## Reverting

### Relevant rules

TODO

### Principles

1. https://en.wikipedia.org/wiki/2010_Flash_Crash
1. https://www.schneier.com/blog/archives/2018/04/tsb_bank_disast.html
1. Revert rollout fast

### Features

1. Automated deploy
1. Revert button

## Reviewing

### Relevant rules

TODO

### Principles


Review apps Great for testing algo changes.

## Deploys

1. Manual deploy (with audit log)
1. Approvers (in branch)

## Security

### Rules

TODO

### Principles

1. Prevent vulnerabilities
1. Make sure all code is scanned
1.

## Features

1. SAST
1. DAST
1. Dependency scanning
1. Container scanning
1. Future: Dashboards

GitLab replaces the following solutions:

1. Sonarqube
1. Jfrog X
1. Blackduck
1. HP Fortify
1. Whitesource
1. Snyk

Also see our [security paradigm](https://about.gitlab.com/handbook/product/#security-paradigm) for more information on why GitLab security tools work better.

## Auditing

1. One concept of a user across the lifecycle ensures the right level of permissions and access
1. Audit logs
1. Container image retention
1. Artifact retention
1. Test result retention
1. Future: Disable squash of commits
1. Future: Prevent purge

## Licensed code

1. License manager

## Disaster recovery

## Rules

## Principles

1. Stay available.
1. No SPOF
1. Quick recovery.
1. Geographic distribution

## Features

1. Geo
1. HA

## State of art

### Rules

TODO

### Principles

1. https://en.wikipedia.org/wiki/State_of_the_art#Tort_liability

## Features

1. GitLab is in use in financial services.
1. Both relevant US regulators run GitLab themselves.
1. 2,000 code contributors
1. 100,000 organizations
1. millions of users

## Interested

Contact sales




## Everyone can contribute

Please email suggestions

MRs are very welcome, assign to.

