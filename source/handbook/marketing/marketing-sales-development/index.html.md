---
layout: markdown_page
title: "Marketing & Sales Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Other Pages Related to Marketing & Sales Development

- [Business Operations](/handbook/business-ops)
- [Reseller Handbook](/handbook/resellers/)

## What is Marketing & Sales Development Handbook

The Marketing & Sales Development department includes the Sales Development, Content Marketing, Field Marketing, Online Growth, and Marketing Operations bands. The bands in this organization employ a variety of marketing and sales crafts in service of our current and future customers with the belief that providing our audiences value will in turn grow GitLab's business.

## Marketing & Sales Development Handbooks

- [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
- [Content Marketing](/handbook/marketing/marketing-sales-development/content)
- [Field Marketing](/handbook/marketing/marketing-sales-development/field-marketing/)
- [Online Growth](/handbook/marketing/marketing-sales-development/online-marketing/)
- [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)
