---
layout: markdown_page
title: "Product Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is Product Marketing?

Product Marketing is GitLab's interface to the market. The market is made up of customers, analysts, press, thought leaders, competitors, etc. Product marketing enables groups like Sales, Marketing, and Channel with narrative, positioning, messaging, and go-to-market strategy to go outbound to the market. Product Marketing does market research to gather customer knowledge, analyst views, market landscapes, and competitor intelligence providing marketing insights inbound to the rest of GitLab.

Product marketing includes:
- [competitive marekting](/handbook/marketing/product-marketing/competitive/)
- partner marketing
- channel marketing
- analyst relations (AR)
- press relations (PR)
- [customer reference program](#customer-reference-program)
- design and dev (Marketing design and marketing website developement)

## Roles

- **PM** - Product Management or Product Manager. The [product team](/handbook/product/) as a whole, or the specific person responsible for a product area.
- **PMM** - Product Marketing Management or Product Marketing Manager. The product marketing team as a whole, or the specific person responsible for a product marketing area.
- PMM Team
  - [Ashish](/team/#kuthiala), Director PMM
  - [John](/team/#j_jeremiah), PMM for [Dev Product Categories](/handbook/product/categories/#dev)
  - [William](/team/#thewilliamchia), [PMM for Ops Product Categories](/handbook/product/categories/#ops), Manager Design & Dev Team
  - [Dan](/team/#dbgordon), Technical PMM, Demos, Competitive
  - [Cindy](/team/cblake2000), PMM for Security Product Categories
  - Joyce, AR & Evangalist Manager
  - Tina, PMM for Partner and Channel
  - [Kim](/team/#kimlock), Customer Reference Manager

- Design & Dev Team
  - [Luke](/team/#lukebabb), Designer
  - [Jarek](/team/#jaaaaarek), Web Developer/Designer

## What is PMM working on?
- View the [Product Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/boards/397296?=&label_name[]=Product%20Marketing) to see what's currently in progress.
- To ask for PMM resources log an issue in the [marketing general project](https://gitlab.com/gitlab-com/marketing/general/issues) and label it with `Product Marketing`.
- Direct and channel sales enablement tasks are tracked on the [Sales Enablement Issue Board](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name[]=Sales%20Enablement).
- To requqest sales enablement topics log an issue in the [marketing general project](https://gitlab.com/gitlab-com/marketing/general/issues) and label it with `Sales Enablement`.
- If you need more immediate attention please send a message in the `#product-marketing` slack channel with an `@reply` to the [PMM responsible](#roles) or you can ping the team with `@pmm-team`.

## Release vs Launch
A [product release, and a marketing launch are two separate activities](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then release it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation.

| Release | Launch |
|-|-|
| PM Led | PMM Led |
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released |

## GitLab Personas

"Personas" are a genearlized way of talking about the people we communicate with
and design for. Keeping the personas in mind with their problems and pain points
allows us to use language that the personas understands. GitLab has both buyer
and user persona types.

### Writing for all personas
- When writing content, always remember to use the [GitLab voice](https://gitlab.com/gitlab-com/marketing/general/blob/master/content/editorial-style-guide.md) regardless of the persona.
- Never alienate other personas. You can appeal to a buyer without buzzword bingo.
- Look at who uses a channel most (twitter & docs: user, webinar linkedin: buyer) when shared (website) guide them /features for users /solutions for buyers.

### User Personas
User personas are people who actually use GitLab. They may or may not be the person in
the organization who has the authority and buget to purchase Gitlab, but they
are heavy influceners in the buying process. User personas are are kept in the [GitLab Design System](https://design.gitlab.com/getting-started/personas).

- [Full-stack web developer](https://design.gitlab.com/getting-started/personas)
- [DevOps Engineer](https://design.gitlab.com/getting-started/personas)
- [Junior web-developer](https://design.gitlab.com/getting-started/personas)

### Buyer Personas

Buyer personas are the people who serve as the main buyer in an organization or
a champion within an enterprise the drives the buying conversation and
coordinates various teams to make a purchase.

1. [Director DevOps video ](https://www.youtube.com/watch?v=qyELotxsQzY), [Director DevOps slide deck](https://docs.google.com/presentation/d/1x-XUhAXkZxl8ZGe4ze9qcWMmrWITc7es1fYNY_OHQQA/) or Director of IT
1. [VP IT video](https://www.youtube.com/watch?v=LUh5eevH3F4), [VP IT Slide Deck](https://docs.google.com/presentation/d/17Ucpgxzt1jSCs83ER4-LdDyEuermpDuriugPNYrz8Rg/) or VP of Engineering
1. [Chief Architect Video](https://www.youtube.com/watch?v=qyELotxsQzY), [Chief Architect slide deck](https://docs.google.com/presentation/d/1KXsozYkimSLlEg3N-sKeN7Muatz_4XimYp-s7_dY1ZM/) or CIO

Also see [the four tiers on our pricing page](https://about.gitlab.com/handbook/product/pricing/#four-tiers).

## Tiers

### Overview

| Tier      | Delivery                  | License                   | Fee            |
| --------- | ------------------------- | ------------------------- | -------------- |
| Core      | Self-hosted               | Open Source               | Gratis         |
| Starter   | Self-hosted               | Source-available          | Paid           |
| Premium   | Self-hosted               | Source-available          | Paid           |
| Ultimate  | Self-hosted               | Source-available          | Paid           |
| Free      | GitLab.com                | Open Source               | Gratis         |
| Bronze    | GitLab.com                | Source-available          | Paid           |
| Silver    | GitLab.com                | Source-available          | Paid           |
| Gold      | GitLab.com                | Source-available          | Paid           |

### Definitions

1. Users: anyone who uses GitLab regardless of tier.
1. Customers: users on a paid tier.
1. Plans: the paid tiers only.
1. Subscription: a combination of the paid tiers and the time-limited tiers, for example the trial tier is a subscription but not a plan.
1. License: open source vs. proprietary, for example moving a feature from a proprietary tier to the open-source tier.
1. Tier: a combination of the subscription tiers and the open source licensed tiers.
1. Distribution: self-hosted CE vs. EE, for example you can have a EE distribution but in the Core tier.
1. Version: the [release of GitLab](/releases/), for example asking what version a user is on.

### Delivery

In general each of the five self-hosted tiers match the features in the GitLab.com tiers. They have different names for two reasons:

1. There is not complete feature parity between self-hosted and GitLab.com plans. For example, Starter, Premium, and Ultimate include [LDAP Group Sync](https://docs.gitlab.com/ee/administration/auth/ldap-ee.html#group-sync) but Bronze, Silver and Gold do not.
1. We want to know if a user is using self-hosted or GitLab.com based on a just the tier name to prevent internal and external confusion.

When we need to say in one word tier a feature is in (for example on our issue tracker) we use the self-hosted tiers because they tend to contain a superset of the GitLab.com tier features.
Where we can we show both the self-hosted and the GitLab.com tiers, do example in [a release post](/2018/02/22/gitlab-10-5-released/#instant-ssl-with-lets-encrypt-for-gitlab).

### Libre, Gratis, and Free
Libre, Gratis, and Free are terms used in the open source community. "Free" is an ambiguous term that can means either free as in "no cost" (e.g. $0 "free as in beer"), free as in "with few or no restrictions" (e.g. "free as in free speech"), or both. "Gratis" is an unambiguous term to mean "no cost" while "Libre" is an unambiguous term to mean "with few no restrictions." Open source software is "libre" in that it is free to inspect, modify, and redistribute. Open source software may or may not be "gratis." Our time-limited tiers are gratis but not open source. Features that are part of our Free and Core tiers refer to open source software that is both [free as in speech and as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). For more info see the [wikipedia article](https://en.wikipedia.org/wiki/Gratis_versus_libre).

### Personal vs Group subscriptions
GitLab.com subscriptions are added to either a personal namespace or a group namespace. Personal subscriptions apply to a single user while Group subscriptions apply to all users in the Group.

### Distributions
Core users can use either one of two distributions: Community Edition (CE) and Enterprise Edition (EE). Enterprise Edition can be downloaded, installed, and run without a commercial subscription. In this case it runs using the open source license and only has access to the open source features. In effect, EE without a subscription, and CE have the exact same functionality. The advantage of using EE is that it is much easier to upgrade to a commercial subscription later on. All that's needed is to install a license key to access more features vs needing to re-install a different distribution.

**Note**: The terms CE & EE refer solely to the software distribution, not to the subscription plan. **Never use CE vs. EE as a substitute for versions.** Instead, talk about open source users vs. [source-available](https://en.wikipedia.org/wiki/Source-available).

### GitLab Trials
Today, we only offer a [free trial for self-hosted GitLab](/free-trial/). The trial offers all of the features of GitLab Ultimate. Users on the Core (self-hosted) and Free (GitLab.com) plans get access for an unlimited amount of time to a limited set of features. Trial users get access to a full set of features for a limited amount of time (30-days).

| License type | Features | Time Period |
| ------------ | -------- | ----------- |
| Core & Free  | Limited (Open source features only) | unlimited |
| Trial        | Unlimited (access to all Ultimate features) | limited (30 days) |

In the future, we might introduce a similar concept for GitLab.com.

### Open source projects on GitLab.com get all Gold features.
The GitLab.com Free plan offers unlimited public and private repos and unlimited contributors but has limited features for private repos. Private repos only get access to the open source features. Public projects get access to all the features of Gold free of charge. This is to show our appreciation for Open Source projects hosted on GitLab.com.

### Messaging dos and don'ts

1. Don't use the terms `Enterprise Edition Starter`, `Enterprise Edition Premium`, `Enterprise Edition Ultimate`, `EES`, `EEP`, or `EEU`. These have all been deprecated.
1. Don't use `Community Edition`, `CE`, `Enterprise Edition`, or `EE` to refer to tiers.
1. Don't use `Community Edition`, `CE`, `Enterprise Edition`, or `EE` to refer to where a feature goes. e.g. "This is a CE feature" or "this is an EE feature."
1. Don't use "edition" to refer to plans like `Starter Edition` or `Premium Edition` - Starter, Premium, and Ultimate are tiers, not "editions" of the software.
1. Don't use "enterprise" as a modifier for tiers such as `Enterprise Starter`, `Enterprise Premium`, or `Enterprise Ultimate`.

1. Do refer to plans by their stand-alone name: Core, Starter, Premium, Ultimate, Free, Sliver, Bronze, and Gold.
1. Do optionally use "GitLab" as a modifier for plan names: GitLab Core, GitLab Starter, GitLab Premium, GitLab Ultimate, GitLab Free, GitLab Sliver, GitLab Bronze, and GitLab Gold.
1. Do use Core, Starter, Premium, Ultimate, Free, Sliver, Bronze, and Gold to refer to where a feature goes. e.g. "This is a Premium feature" or "We are moving this feature from Premium & Silver to Core & Free"
1. Do use "enterprise" to describe a market segment. e.g. good phrases to use are: "GitLab provides DevOps for the enterprise", "GitLab is enterprise-ready", "GitLab has many enterprise customers", and "GitLab provides enterprise software for the complete DevOps lifecycle."
1. Do use `Community Edition`, `CE`, `Enterprise Edition` and `EE` to refer to our software distributions. Encourage customers to use the EE distribution since it provides the least painful upgrade path if/when users discover they need commercial features.
1. Do use the terms "Open Source" and "Source-available". Unlike other terms that are GitLab-insider language (e.g. "Free" vs "Bronze") almost everyone will immediately understand the difference. Calling our proprietary tiers closed source doesn't make sense because even our proprietary source code is publicly viewable.

## Elevator pitch

**The Problem - Customer Perspective**

Right now every large enterprise is suffering from a lack of consistency:

* Not using the same tools
* Not using the same integrations
* Not using the same configuration
* Not using the same work processes
* Not being judged on the same metrics

And they have processes which block reducing time to value, for example:

* Security reviews that are blocking until approved
* Infrastructure that has to be provisioned
* Fixed release windows
* Production that needs to approve releases
* SOX compliance sign offs that need to happen
* QA cycles
* Separate QA teams
* Separate build teams

**Solution**

### Single Sentence

GitLab is the first single application for software development, security, and operations that enables Concurrent DevOps, making the software lifecycle 3 times faster and radically improving the speed of business.

### Short Message
(~50 words)

GitLab is the first single application for all stages of the DevOps lifecycle. Only GitLab enables Concurrent DevOps, unlocking organizations from the constraints of the toolchain. GitLab provides unmatched visibility, higher levels of efficiency, and comprehensive governance. This makes the software lifecycle 3 times faster, radically improving the speed of business.

### Medium Message
(~250 words)
GitLab is the first single application for all stages of the DevOps lifecycle. Only GitLab enables Concurrent DevOps, unlocking organizations from the constraints of today’s toolchain. GitLab provides unmatched visibility, radical new levels of efficiency and comprehensive governance to significantly compress the time between planning a change and monitoring its effect. This makes the software lifecycle 3 times faster, radically improving the speed of business.

GitLab and Concurrent DevOps collapses cycle times by driving higher efficiency across all stages of the software development lifecycle. For the first time, Product, Development, QA, Security, and Operations teams can work concurrently in a single application. There’s no need to integrate and synchronize tools, or waste time waiting for handoffs. Everyone contributes to a single conversation, instead of managing multiple threads across disparate tools. And only GitLab gives teams complete visibility across the lifecycle with a single, trusted source of data to simplify troubleshooting and drive accountability. All activity is governed by consistent controls, making security and compliance first-class citizens instead of an afterthought.

Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel trust GitLab to deliver great software at new speeds.

### Long Message
(~450 Words)
GitLab is the first single application for software development, security, and operations that enables Concurrent DevOps, making the software lifecycle 3 times faster and radically improving the speed of business. Only GitLab provides a single application that unlocks organizations from the compromises and constraints of today’s DevOps Toolchain, significantly improving visibility, efficiency and governance. Now, fast paced teams no longer have to integrate or synchronize multiple DevOps tools and are able to go faster by working seamlessly across the complete lifecycle.

GitLab delivers complete real-time visibility of all projects and relevant activities across the expanded DevOps lifecycle. For the first time, teams can see everything that matters. Changes, status, cycle times, security and operational health are instantly available from a trusted single source of data. Information is shown where it matters most, e.g. production impact is show together with the code changes that caused it. And developers see all relevant security and ops information for any change. With GitLab, there is never any need to wait on synchronizing your monitoring app to version control or copying information from tool to tool. GitLab frees teams to manage projects, not tools. These powerful capabilities eliminate guesswork, help teams drive accountability and gives everyone the data-driven confidence to act with new certainty.  With Gitlab, DevOps teams get better every day by having the visibility to see progress and operate with a deeper understanding of cycle times across projects and activities.

GitLab drives radically faster cycle times by helping DevOps teams achieve higher levels of efficiency across all stages of the lifecycle. Concurrent DevOps makes it possible for Product, Development, QA, Security, and Operations teams to work at the same time, instead of waiting for handoffs. Teams can work concurrently and review changes together before pushing to production. And everyone can contribute to a single conversation across every stage. Only GitLab eliminates the need to manually configure and integrate multiple tools for each project. Teams can start immediately and work concurrently to radically compress time across every stage of the DevOps lifecycle.

Only GitLab delivers DevOps teams powerful new governance capabilities embedded across the expanded lifecycle to automate security, code quality and vulnerability management. With GitLab, tighter governance and control never slow down DevOps speed.

GitLab leads the next advancement of DevOps. Built on Open Source, GitLab  delivers new innovations and features on the same day of every month by leveraging contributions from a passionate, global community of thousands of developers and millions of users. Over 100,000 of the world’s most demanding organizations trust GitLab to realize the transformative power of Concurrent DevOps to achieve a 3x faster lifecycle.

### Is it similar to GitHub?

GitLab started as an open source alternative to GitHub. Instead of focusing on hosting open source projects we focused on the needs of the enterprise, for example we have 5 authorization levels vs. 2 in GitHub. Now we've expanded the feature set with continuous integration, continuous delivery, and monitoring.

### What is the benefit?

We help organizations go faster to market by reducing the cycle time and simplifying the toolchain development and operations use to push their software to market. Before GitLab you needed 7 tools, it took a lot of effort to [integrate](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) them, and you end up with have different setups for different teams. With GitLab you gain visibility on how long each part of the software development lifecycle is taking and how you can improve it.

### Email Intro

GitLab makes it easier for companies to achieve software excellence so that they can unlock great software for their customers by reducing the cycle time between having an idea and seeing it in production. GitLab does this by having an [integrated product](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) for the entire software development lifecycle. It contains not only issue management, version control and code review but also Continuous Integration, Continuous Delivery, and monitoring. Organizations all around the world, big and small, are using GitLab. In fact, 2/3 of organizations that self-host git use GitLab. That is more than 100,000 organizations and millions of users.

### Not a Platform

GitLab is not a platform, it's a single application.

A platform implies a surface on which smaller applications are run or integrated [see the dictionary.com definition](http://www.dictionary.com/browse/software-platform).
Rather, GitLab is a single application that offers the features of many applications through a single interface.

This is a unique property in comparison with our competitors, with [many benefits](/handbook/product/single-application).

### [GitLab Positioning](/handbook/positioning-faq/)

### [GitLab Demo](/handbook/marketing/product-marketing/demo/)



## Customer Reference Program

The goal of our Customer Reference Program is to provide opportunities for customers to share their story on how GitLab has helped them overcome the challenges, blockers and pain points within their organizations.


The goals of the Gitlab Customer Reference Program include:
- **Credibility** -Reinforce our credibility as an end-to-end DevOps solution partner.
- **Shorten the sales cycle** - Having our advocates involved with sharing their story with potential sales, analysts and the marketplace can help potential sales close sooner
- **Increase Revenue** - A successful Customer Reference Program helps increase revenue by attracting new clients and increasing retention rates.
- **Customer Advocacy** - Encourage our customers to share their success stories with others who are learning about or evaluating GitLab.

### Customer Reference Types
Some examples of the types of assets we'd use as customer references once we have approval from the customer:
* Logo (The ability to name a company as a customer and use their logo in our marketing materials)
* Live sales reference (both calls and possible in-person)
* Website content
  * Case studies
  * Blog posts
  * Videos
  * Podcasts
* Event speakers (At industry, third-party and company events)
* References for analysts and press
* Quotes (can be attributable with approval or anonymous)
* Anecdotes (short "snackable" stories, can be attributable with approval or anonymous) Read [GitLab's customer anecdotes](#customer-anecdotes).

### Customer Case Studies
The most recent customer case studies are found on the [GitLab customer's page.](https://about.gitlab.com/customers/)

### Customer references repository

Today, we don't have a central repository for customer service, but we are starting to build one. For now we are capturing customer stories in an issue. If you have a customer story or anecdote [leave a comment on issue 1834](https://gitlab.com/gitlab-com/marketing/general/issues/1834).

To initiate a formal case study process, follow the process listed [here](https://about.gitlab.com/handbook/marketing/marketing-sales-development/content/case-studies/)

### Reference Program Process
1. Adding a new reference customer
    1. AE nominates customer for the program to Customer Reference Manager
    2. CRM - AE briefing <br>
Key info needed:
		- Industry / Vertical
		- Region/Geo
    - Tier (core, starter, premium, ultimate)
    - [GitLab Use Case] (https://about.gitlab.com/handbook/use-cases/)
    - Customer Segment (strategic, large, smb)
    - Deployment size (licenses)
    - Customer Story (why GitLab, key metrics, etc)
    - Who did we beat?<br>
 	 3. AE Introduces CRM to customer
		- Describe the Customer Reference program
    - Describe the types of reference activities and gauge their interest
		- Gauge their interest participation level

### Sample Interview Questions/topics
    * What led you to GitLab, what problems were you trying to solve?
    * Why did you choose GitLab and what other tools were you using or considering?
    * What has been your experience with GitLab?  
    * How did you make the business case for GitLab and what metrics have you seen improve?
    * What have you heard from the GitLab users, what was the adoption curve like? <br>
    4. Update Customer Reference Spreadsheet (future SFDC) with details from interview to make sure the customer is identified as a potential reference and their desired activity level.

### GitLab DevOps Customer Advisory Board
***Purpose:*** To help foster DevOps transformation and adoption we are establishing a Customer Advisory Board, where we focus on sharing DevOps best practices and lessons learned with each other.   We believe that transparency and sharing  is a key way to help encourage the success of DevOps transformations.  The GitLab customer advisory board is intended to be home to learning and collaboration so we can all experience success through DevOps transformation.

***Members*** Executives/Champtions for DevOps within their organizations

***Frequency:***  We will try to meet virtually every 6 to 8 weeks

***Membership:*** Approximately 15 - 20 customers, GitLab

***Open by default:*** In alignment with GitLab values, our CAB discussions and sharing will be open and transparent. We intend to livestream the meetings. (Anonymized in the meeting)

### GitLab Special Interest Group

***Purpose:*** We are forming Special Interest Groups to foster specific and focused discussions about how to apply DevOps practices and GitLab capabilities in specific domains such as planning, development,  CI/CD, security, etc. These Special Interest Groups will encourage sharing and collaboration of DevOps best practices and lessons learned between users and GitLab.

We believe that transparency and sharing is a key way to help us all learn and improve how we deliver for our customers. GitLab special interest groups are intended to be home to learning and collaboration.

***Members*** Technical utilizers and advocates of GitLab

***Frequency:***  We will try to meet virtually every 6 to 8 weeks

***Membership:*** Approximately 8-15 users, GitLab Product Manager

***Open by default:*** In alignment with GitLab values, our SIG discussions and sharing will be open and transparent. We intend to livestream the meetings. (Anonymized in the meeting)

### Customer Anecdotes
Customer anecdotes are short, "snackable" customer stories. These can be used on a call or in a meeting to share a point about GitLab by validating it with the customer story. An anecdote can be a summary of a formal, attributable case study or just an annonymouse story from a conversation you had with a customer. It's important to keep customers annonymous unless we have explicit approval to use their name.

#### Administrative overhead
We hosted a dinner for customers and prospects to mingle with each other and share stories. When the topic of managing the DevOps toolchain came up, the Head of Risk at a large US bank mentioned that she had a team of 20 to keep SDLC tools running in her org. A GitLab customer, the managing director of a product group for a global investment management corporation said, "this is going to break your heart, but I have 1 guy that spends 25% time to keep GitLab up and going for my team of 1500 devs."

#### Adoption
A large enterprise in the software space is a recent GitLab Premium customer wanting to replace Perforce. They predicted that it would take them 3 years to hit 6K active users. But, the developer experience was so superior that they ended up hitting 6K active users in only 8 months.

#### Ticketmaster - 15x Faster Build time
Ticketmaster is the global event ticketing leader with one of the world's top five ecommerce sites, getting almost 27 million monthly unique visitors. Ticketmaster was using Jenkins for continuous integration. Weighed down by plug-ins and legacy development, their pipeline was taking 2 hours to complete. After getting stuck late on a Friday night waiting for the build to complete their ops team got fed up and began to explore other options. They were able to run their pipeline on GitLab CI/CD in 8 minutes for a 15x increase.

https://about.gitlab.com/2017/06/07/continous-integration-ticketmaster/

#### Axway - 26x faster release cycles
Axway, is a global enterprise software company with over €300 million in yearly revenue. Axway wanted to adopt DevOps practices but their legacy Subversion toolchain was blocking them. By moving from Subversion to Git using GitLab as their Source Code Management (SCM) solution they were able to implement DevOps integrating GitLab into tools like JIRA, for issue management and Jenkins for continuous integration. They increased demployents from once-a-year to every two weeks.

https://about.gitlab.com/customers/axway/

#### Paessler - 120X Increased QA efficiency
Paessler AG provides the award-winning PRTG Network Monitoring software used by over 150,000 IT administrators in more than 170 countries. QA engineers were manually testing software with a routine set of tasking taking an hour to complete. By implementing GitLab pipelines they were able to automate QA tasks requiring only 3 minutes of effort for a 120x efficiency increase.

https://about.gitlab.com/customers/paessler/

#### Equinix -  increased DevOPs agility
Equinix is a leading global data center company . Their client-side development teams responsible for building software products and business critical applications increase development speed, self-serviceability and ability to ship fixes and features quickly. Equinix needed a version control and continuous integration tool for distributed workflows to support globally distributed development teams.

https://about.gitlab.com/customers/equinix


## Print Collateral

- [GitLab DataSheet](/images/press/gitlab-data-sheet.pdf)
- [GitLab Federal Capabilities One-pager](/images/press/gitlab-capabilities-statement.pdf)

## Marketing Decks

### Company Pitch Deck
The [Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/) contains the GitLab narrative and pitch.

### Security Deck
This [Security Deck](https://docs.google.com/presentation/d/1lNr9pz7axLlN7uw7Wkwi_FYMuEh4F4QzPaoJLfReGFk/edit#slide=id.g2823c3f9ca_0_9) introduces GitLab's position and capabilities around security. It covers why better security is needed now and how GitLab provides that better security in a more effective manner than traditional tools. This deck should be used when talking to prospects who are asking about how GitLab can help them better secure their software.

A uniform lexicon is important to distinguish the use of 'security' in various contexts.

1.  GitLab is a **Secure Application** (used as an adjective like GitLab is scalable, open, etc.) The security team manages people, processes and technology to secure the GitLab software that may include SAST & DAST but also includes security policies (like using Macs), our own Security Controls, configurations, monitoring of GitLab in production, vulnerability management, etc.
1.  GitLab helps our customers **Secure and Manage** all of the phases of the SDLC Create, Plan, etc.). To deliver secure applications, customers use GitLab Security Controls throughout the SDLC and Security Testing in validation. Eventually, GitLab will enable vulnerability prioritization for planning and Security Monitoring in production.
  a. **Security Testing** is a capability or feature of GitLab, typically used in the Verify phase. It includes SAST and DAST, container scanning and dependency scanning (@plafoucriere, @bikebilly  and team).
  b. **Security Controls** are capabilities of Gitlab that altogether provide GitLab customers auditability of code throughout the SDLC. (This is NOT SAST/DAST.) GitHub describes theirs https://docs.google.com/document/d/1s5RIE8hFaMdoBqrnLVlnbuxDUQfZd1kYjduVOZCWIKE/edit?usp=sharing
    * Enforce security policies without interrupting your workflow
    * Complete change log for auditing
    * Two-factor authentication (2FA) for added access control
    * Automated security scanning during verification

### Marketing Deck Change process
Marketing decks linked on this page are the latest approved decks from Product Marketing that should be always be in a state that is ready to present. As such there should never be comments or WIP slides in a marketing deck. Only the PMM team and CMO have write access to the marketing decks. Changes, comments, and additions should be made via this process:
1. Create a google slide deck with the slide(s) you want to update
1. Create an issue in the [marketing issue tracker](https://gitlab.com/gitlab-com/marketing/general/issues)
1. Add a reason for the proposed changes in the description
1. Label the issue with "Product Marketing"

Discussion takes place in the issue and revisions are made on the slide until it is ready to move to the marketing deck. When it is ready, a PMM will move the slide into the marketing deck and close the issue.

## Press release boiler plate
GitLab is a single application built from the ground up for all stages of the DevOps lifecycle for Product, Development, QA, Security, and Operations teams to work concurrently on the same project.  GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle allowing teams to collaborate and work on a project from a single conversation, significantly reducing cycle time and focus exclusively on building great software quickly.  Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel trust GitLab to deliver great software at new speeds.

## Analyst product categorizations

GitLab is a single application that spans many product categories.
Forrester and Gartner define several of these categories and their definition
of a space can be useful to determine what important competing products are.
Below, some relevant categories are listed that GitLab is or will be competing in.

### Forrester: Continuous Integration Tools

- Leaders: GitLab CI, CloudBees Jenkins, CircleCI, Microsoft
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Integration+Tools+Q3+2017/-/E-RES137261)

### Forrester: Configuration Management Software For Infrastructure Automation

- Leaders: Puppet Enterprise, Chef Automate
- [Link to report](https://reprints.forrester.com/#/assets/2/675/'RES137964'/reports)

### Gartner: Software Change and Configuration Management Software

- Market Guide: Amazon Web Services, Atlassian, BitKeeper, CA Technologies, Codice Software, Collabnet, GitHub, GitLab, IBM, Micro Focus/Borland, Microsoft, Perforce, PTC, SeaPine Software, Serena, SourceGear, Visible Systems, WANdisco, Wildbit
- [Link to report](https://www.gartner.com/document/3118917)
- Market Guide update initiated March 2018.

### Gartner: Continuous Configuration Automation Tools

- Market Guide: Chef, CFEngine, Inedo, Orca, Puppet, Red Hat, SaltStack
- [Link to report](https://www.gartner.com/document/3843365)

### Gartner: Software Test Automation

- Leaders: MicroFocus, Tricentis
- [Link to 2016-11-15 report](https://www.gartner.com/document/3512920)
- [Link to 2017-11-20 report](https://www.gartner.com/document/3830082)

### Forrester: Modern Application Functional Test Automation Tools

- Leaders: Parasoft, IBM, Tricentis, HPE
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Modern+Application+Functional+Test+Automation+Tools+Q4+2016/-/E-RES123866)

### Gartner: Performance Testing

- Market Guide: Automation Anywhere, BlazeMeter, Borland, CA Technologies, HPE, IBM, Neotys, Oracle, Parasoft, RadView, SmartBear, Soasta, Telerik, TestPlant
- [Link to report](https://www.gartner.com/document/3133717)

### Gartner: Application Release Automation

- Leaders: Electric Cloud, CA (Automic), XebiaLabs, IBM
- [Magic quadrant](http://electric-cloud.com/resources/whitepapers/gartner-magic-quadrant-application-release-automation/)
- MQ update underway. Research kicked off March 2018


### Forrester: Continuous Delivery and Release Automation

- Leaders: Chef, CA, Microsoft
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Delivery+And+Release+Automation+Q3+2017/-/E-RES137969)

### Gartner: Application Performance Monitoring Suites

- Leaders: New Relic
- [Link to report](https://www.gartner.com/doc/3551918)

### Gartner: Application Security Testing

- Leaders: Micro Focus, CA Technologies (Veracode), Checkmarx, Synopsys, IBM
- [Link to 2017-02-28 report](https://www.gartner.com/doc/3623017)
- [link to 2018-03-19 report](https://www.gartner.com/doc/3868966/magic-quadrant-application-security-testing)

### Forrester: Application Security Testing

- Leaders: Synopsys, CA Veracode
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Static+Application+Security+Testing+Q4+2017/-/E-RES139431)

### Gartner: Project Portfolio Management

- Leaders: Planview, CA Technologies, Changepoint
- [Link to report](https://www.gartner.com/document/3728917)

### Forrester: Strategic Portfolio Management Tools

- Leaders: AgileCraft, CA, ServiceNow
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Strategic+Portfolio+Management+Tools+Q3+2017/-/E-RES136707)

### Forrester: Portfolio Management For The Tech Management Agenda

- Leaders: CA, Planview
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Portfolio+Management+For+The+Tech+Management+Agenda+Q1+2015/-/E-RES114742)

### Gartner: Enterprise Agile Planning Tools

- Leaders: CA, Atlassian, VersionOne
- [Link to report](https://www.gartner.com/doc/3695417)
- Research for next MQ commences Summer 2018

### Forrester: Enterprise Collaborative Work Management

- Leaders: Clarizen, Redbooth, Wrike, Planview, Asana, and Smartsheet
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Enterprise+Collaborative+Work+Management+Q4+2016/-/E-RES121721)

### Forrester: Application Life-Cycle Management, Q4 2012

- Covered in report: Atlassian, CollabNet, HP, IBM, Microsoft, PTC, Rally Software, Rocket Aldon, and Serena Software
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Application+LifeCycle+Management+Q4+2012/-/E-RES60080)

### Gartner: Container Management Software

- Market Guide: Apcera, Apprenda, CoreOS, Docker, Joyent, Mesosphere, Pivotal, Rancher Labs, Red Hat
- [Link to report](https://www.gartner.com/document/3782167)

### Gartner: Enterprise Application Platform as a Service

- Leaders: Salesforce, Microsoft
- [Link to report](https://www.gartner.com/document/3263917)

### Gartner: Data Science Platforms

- Leaders: IBM, SAS, RapidMiner, KNIME
- [Link to report](https://www.gartner.com/doc/3606026)

## Partner Marketing

### Partner Marketing Objectives

- Promote existing partnerships to be at top-of-mind for developers.
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

For a list of Strategic Partners, search for "Partnerships" on the Google Drive.

### Partner Marketing Activation

Our partner activation framework consists of a series of action items within a high-level issue. When a strategic partnership is signed, Product Marketing will choose the issue template called [partner-activation](https://gitlab.com/gitlab-com/marketing/general/issues/new) which will trigger notification for all involved in partner activation activities.

For each action item within this issue, a separate issue should be created by the assignee who is responsible for that action item within the allocated timeframe.

The partner should be included on the high level issue so they can see the planned activities and can contribute.

### Partner Newsletter Feature

In line with the objective of "Promote existing partnerships to be at top-of-mind for developers", a regular feature in our fortnightly (8th & 22nd) newsletter will promote our partners to our target audience. This feature should be co-authored by the partner.

Possible content:

- Feature on new partner if signed
- Feature on existing partner if major update released
- Feature on existing partner highlighting benefits of partner product
- 1-minute video showcasing the integration as a reference
- Blog post from partner
- Feature on how existing customer uses GitLab and partner

Suggested format:

- Length: Couple of paragraphs
- Links: GitLab integration/partner page & partner website

Creation:

Email potential partner with case for creating content/blog post which will feature in our newsletter. Also request that they include the content in their own newsletter.

Create separate issue for blog post in www-gitlab-com project with blog post label and assign to Content Marketing with the following information:
- A summary describing the partnership
- Any specific goals for the blog post (e.g. must link to this, mention this, do not do this...)
- Contacts from both sides for people involved in the partnership
- Any marketing pages or material created from both sides which we can use in the post
- Deadlines for the post (internal draft, partner draft review and publication)

After publication: Send to partner a summary of the click-throughs to their website, registration for webinar etc.

## Channel Marketing

### Channel Marketing Objectives

- Support resellers to be successful and grow their business
- Motivate resellers to reach first for GitLab
- Promote [Reseller program](/resellers/program/) to recruit new resellers

### Reseller Funds Allocation Determination

- Resellers will request event, SWAG or campaign support by creating an issue using the [reseller issue template](https://gitlab.com/gitlab-com/resellers/issues/new). When the reseller has completed the issue template detailing their needs, Product Marketing and the Channel Reseller Director will be notified.
- When a reseller requests funds for online marketing campaigns, let them know that we can run the campaign in-house working with the Demand Generation team, and even provide artwork if required. All we need is the redirect links to the: /gitlab.com on their website.
- Post-event or campaign, set up a catch-up call with the reseller to determine ROI of GitLab investment.
- At the post event catch-up with the reseller, there are two tabs in the "GitLab events sponsorship request form (Responses)" sheet found on the Google Drive to be updated - one for events and the other online marketing campaigns.
- Request that the reseller send photos of the event, write a couple of short paragraphs on their experience at the event, any highlights, and impact on their business and GitLab, and send to you. Photos and a short blog post could feature in the Reseller newsletter, the company-wide newsletter.
- After the Online Marketing campaign, we will send the reseller a summary from Google Adwords or equivalent with the metric measurements detailing the success of the campaign. Store campaign summaries in the "Online Marketing Campaign Summaries" folder on the Google Drive.
- As we gather more feedback, we will be able to assign a ROI to our marketing investments, and drive more SQLs.

## Verticals
Various verticals and industries face specific challenges as they address their specific market.  Ranging from compliance and regulatory pressure to specific market disruptions, IT teams in these verticals are developing solutions to their unique challenges.

 -  Automotive
 -  Health care
 -  Financial services
 -  Federal/Govt/ Pub Sector
 -  Retail
 -  Advanced Manufacturing (as opposed to automotive)
 -  Oil & Gas/Energy

### Automotive

| Challenge | GitLab Helps |
| --------- | ------------ |
| **The connected car (over the air updates):** As vehicles become mobile computers on wheels, the importance and cost of updating on board software is a key issue.  It is expensive and disruptive to require owners to have updates installed by dealers, therefore the industry is moving to an approach that enables over the air updates. | To support over the air updates, automobile manufacturers and their suppliers will depend on rapid and efficient software delivery toolchains that streamline development, testing and even deployment of software changes. |
| **Transportation as a Service** | In this market transformation, the differentiation will be in the quality, simplicity and adoption of the technology platform that enables transportation as a service. This will demand rapid and responsive design, development and iteration from the software teams building the transportation as a service platform and infrastructure. (GitLab/Concurrent devops) |
| **Platform consolidation** | In order to reduce manufacturing complexity and waste, platform consolidation will drive manufacturers to differentiate their products on the underlying technology and consumer services (software). The software will be the differentiator, and will require rapid innovation to keep up with a rapidly changing market. |

- http://cdn.ihs.com/www/pdf/AUT-TL-WhitePaper-5.pdf
- http://www.digitalistmag.com/digital-economy/2017/09/14/top-tech-challenges-disrupting-revolutionizing-auto-industry-05365751
- https://www.strategyand.pwc.com/trend/2017-automotive-industry-trends

### Health care

| Challenge | GitLab Helps |
| --------- | ------------ |
| **Security & Data Protection** Ensuring that their systems and data are secure is critical in health care.  This is an issue across the entire lifecycle of health care systems from development to production and is a top issue for  most all healthcare IT leaders. | A single platform that controls and manages  access to the the SDLC tool chain.    Built in audits, reviews and automation help to create a trusted pipeline.  |
| **Patient Experience** The healthcare market is being dramatically transformed around improving patient experience, making it easier for patients to participate in their care.  Ranging from mobile applications to electronic health records and other innovation, healthcare IT leaders are trying to accelerate delivery and iteration are to improve patient experience. | Rapid delivery and iteration are the hallmarks of improving patient experience.  Innovation in UX is only possible if IT teams are able to rapidly learn and innovate.  Concurrent Devops and GitLab help to streamline delivery and enable IT teams to focus on improving patient experience. |
| **HIPAA** | A single platform that controls and manages  access to the the SDLC tool chain.    Built in audits, reviews and automation help to create a trusted pipeline. |

- https://www.pwc.com/us/en/health-industries/top-health-industry-issues.html
- https://www.pwc.com/us/en/health-industries/assets/pwc-health-research-institute-top-health-industry-issues-of-2018-report.pdf
- https://www.healthdatamanagement.com/opinion/how-3-major-challenges-will-shape-healthcare-it-in-2018
- https://thedoctorweighsin.com/four-it-challenges-facing-healthcare-organizations-in-2018/
- https://www.advisory.com/research/health-care-it-advisor/it-forefront/2018/01/top-it-2018


### Financial Services

| Challenge | GitLab Helps |
| --------- | ------------ |
| **Security** | GitLab’s single platform controls and manages access to the SDLC tool chain helping them to increase confidence in their development process.   The ability to audit, review and automate security scans as a core part of the development lifecycle further helps to ensure that every application they build is more secure. |
| **Aging infrastructure** | Gitlab simplifies move to modern cloud native (kubernetes) |
| **Compliance** | A single platform that controls and manages  access to the the SDLC tool chain.    Built in audits, reviews and automation help to create a trusted pipeline. |
| **New technology (business disruption / digital transformation)** | This will demand rapid and responsive design, development and iteration from the software teams building the transportation as a service platform and infrastructure.   GitLab and concurrent devops can help teams to dramatically decrease cycle times to ship software. |

- https://www.cio.com/article/3128314/financial-it/8-challenges-that-keep-financial-services-ctos-and-cios-up-at-night.html
- https://www.pwc.com/us/en/industries/financial-services/research-institute/top-issues.html
- https://www.pwc.com/us/en/financial-services/research-institute/assets/pwc-fsi-top-issues-2018.pdf
- https://www.protiviti.com/sites/default/files/united_states/insights/fs-insights-v4-i3-protiviti.pdf

###  Oil & Gas/Energy

| Challenge | GitLab Helps |
| --------- | ------------ |
| **Digital Oilfield** |  Speed and velocity to innovate            |
| **Efficiency in an dynamic commodity market**  |  Speed to innovate and automation to reduce costs            |
| **Need for scalable compute to process big data** |  Gitlab support for kubernetes            |

- https://www.stoutadvisory.com/insights/article/trends-and-challenges-oil-and-gas-industry
- http://www-935.ibm.com/services/us/gbs/bus/pdf/g510-3882-meeting-challenges-oil-gas-exploration.pdf
- https://smart-grid.energycioinsights.com/cxo-insights/how-technology-is-changing-the-oil-and-gas-landscape-for-the-better-nwid-51.html
