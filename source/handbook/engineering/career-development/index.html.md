---
layout: markdown_page
title: "Engineering Career Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Individual Contribution vs. Management

Most important is the fork between purely technical work and managing teams. It's important that Engineering Managers self-select into that track and don't feel pressured. We believe that management is a craft like any other and requires dedication. We also believe that everyone deserves a manager that is passionate about their craft.

Once someone reaches a Senior-level role, and wants to progress, they will need to decide whether they want to remain purely technical or pursue managing technical teams. Their manager can provide opportunities to try tasks from both tracks if they wish. Staff-level roles and Engineering Manager roles are equivalent in terms of base compensation and prestige.


## Roles

<table style="text-align:center; border-spacing:5px; border-collapse:separate; font-size:14px;">
  <tr>
    <td colspan="6" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/developer#staff-developer">Staff Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/developer#distinguished-developer">Distinguished Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/developer#engineering-fellow">Engineering Fellow</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/chief-technology-officer/">Chief Technology Officer</a></td>
  </tr>
  <tr>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/developer#junior-developer">Junior Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/developer#intermediate-developer">Intermediate Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/developer#senior-developer">Senior Developer</a></td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/engineering-management#engineering-manager">Engineering Manager</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/engineering-management#director-of-engineering">Director of Engineering</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/engineering-management#vp-of-engineering">VP of Engineering</a></td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/security-engineer/#staff-security-engineer">Staff Security Engineer</a></td>
  </tr>
  <tr>
    <td rowspan="2" colspan="2" style="border:0;"></td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/security-engineer/#intermediate-security-engineer">Security Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/security-engineer/#senior-security-engineer">Senior Security Engineer</a></td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/security-management#security-engineering-manager">Security Engineering Manager</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/security-management#director-of-security">Director of Security</a></td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;">Staff Automation Engineer</td>
  </tr>
  <tr>
    <td rowspan="2" colspan="2" style="border:0;"></td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/automation-engineer">Automation Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;">Senior Automation Engineer</td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/manager-test-automation">Manager, Test Automation</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;">Director of Quality</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" style="border:0;"></td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/services-supportt#staff-services-agent">Staff Services Support Agent</a></td>
  </tr>
  <tr>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/services-support#intermediate-support-agent">Intermediate Services Support Agent</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/services-support#senior-support-agent">Senior Services Support Agent</a></td>
    <td rowspan="1" style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/support-management#services-support-manager">Services Support Manager</a></td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
 <tr>
    <td colspan="6" style="border:0; vertical-align:middle;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/support-engineer#staff-support-engineer">Staff Support Engineer</a></td>
  </tr>
  <tr>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/support-engineer#junior-support-engineer">Junior Support Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/support-engineer#intermediate-support-engineer">Intermediate Support Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/support-engineer#senior-support-engineer">Senior Support Engineer</a></td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/support-management#support-engineering-manager"> Support Engineering Manager</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/job-families/engineering/support-management#director-of-support">Director of Support</a></td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
</table>

## Internships

We normally don't offer any internships, but if you get a couple of merge requests
accepted, we'll interview you for one. This will be a remote internship without
supervision; you'll only get feedback on your merge requests. If you want to
work on open source and qualify please submit an application.
In the cover letter field, please note that you want an internship and link to
the accepted merge requests. The merge requests should be of significant
value and difficulty, which is at the discretion of the manager. For
example, fixing 10 typos isn't as valuable as shipping 2 new features.

## Junior Developers

We will occasionally hire a Junior Developer if we feel that they are capable of
quickly becoming an exceptional member of our team, but lacking in some of the
necessary skills for being hired at an Intermediate or higher level position.
There are [clear expectations and support provided](/handbook/engineering/career-development/junior-developers) to help ensure
that they gain these skills as efficiently as possible.

> Note: We are currently focused on rapid scaling and are not likely to consider internships or junior roles at this time.

## Senior Developers

Note that we have a specific section for Senior Developer because it's an important step in the technical development for every engineer. But "Senior" can optionally be applied to any role here indicating superior performance. However, it's not required to pass through "senior" for roles other than Developer. 

Senior developers typically receive fewer trivial comments on their merge requests. Attention to detail is very important to us. They also receive fewer _major_ comments because they understand the application architecture and select from proven patterns. We also expect senior developers to come up with simpler solutions to complex problems. Managing complexity is key to their work.

## Trying the Management Track

People deserve great managers. It's important that people interested in the management track have opportunities to try it out prior to committing themselves. Managers can provide multiple opportunities to Senior and Staff Developers who want to consider moving into an available or upcoming manager role. Examples include delivering an functional group update (FGU), acting as the hiring manager for an intern position, or running a series of demo meetings for an important deliverable. This is also very important for the individual. If someone finds out they don't like management (or don't have the aptitude) after accepting the title, they might feel like they have lost face when they return to their previous position. This is unnecessary and unfortunate, because management is a craft like many others and it's not for everyone.

In order to facilitate this transition, we recommend any person moving from an
Individual Contributor role to a Management role work with their manager to
create a focused transition plan. The goal is to provide a concentrated look
into the responsibilities and challenges of management with the understanding
that, if the role is not for them, they can return to the IC track. A good
example of how to build such a plan can be found in [this
article](http://firstround.com/review/this-90-day-plan-turns-engineers-into-remarkable-managers/).  Another good resource for when you are ready for the critical career path conversation is [this 30 mins video](https://www.youtube.com/watch?reload=9&v=hMz6QDURQOM&list=PLBzScQzZ83I8H8_0Qete6Bs5EcW3p0kZF&index=6). 


## Promotion

We strive to set the clearest possible expectations with regard to performance and [promotions](/handbook/people-operations/promotions-transfers/). Nevertheless, some aspects are qualitative. Examples of attributes that are hard to quantify are communication skills, mentorship ability, accountability, and positive contributions to company culture and the sense of psychological safety on teams. For these attributes we primarily rely on the experience of our managers and the [360 feedback](/handbook/people-operations/360-feedback/) process (especially peer reviews). It's our belief that while a manager provides feedback and opportunities for improvement or development, that it's actually the team that elevates individuals.

#### Transfer Options

The following table outlines the lateral transfer options at any level of the role. Experience factor might differ per individual to determine leveling for each of the positions listed.

| Starting Role                        | Lateral Options                   |
|--------------------------------------|-----------------------------------|
| Frontend Engineer                    | UX Designer                       |
| UX Designer                          | Frontend Engineer                 |
| Developer                            | Production Engineer               |
| Production Engineer                  | Developer                         |
| Developer                            | Support Engineer                  |
| Support Engineer                     | Developer                         |
| Support Engineer                     | Solutions Architect               |
| Support Engineer                     | Implementation Specialist         |
| Automation Engineer                  | Developer                         |
| Developer                            | Automation Engineer               |

Specific backend teams can also be looked at for a lateral transfer. Those teams include Build, Platform, CI/CD, Geo, Monitoring, Gitaly, etc.
